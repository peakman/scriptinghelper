//
//  scripting_app.swift
//  ScriptingHelper
//
//  Created by Steve Clarke on 26/07/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

import Foundation
import ScriptingBridge
import ScriptingHelperOC


public class ScriptingApp  {
    struct Messages {
        static let appMustExist: StaticString = "Application must exist"
    }
    public let appName : String
    public let appPath : String
    public var app : AnyObject {
        var scApp = SBApplication(URL: NSURL.fileURLWithPath("\(appPath)/\(appName).app"))
        //scApp.activate()
        return scApp
    }
    public init(_ appName : String , path: String = "/Applications") {
        self.appName = appName
        self.appPath = path
    }
    
    public func quitWithoutSaving() {
        app.quitSaving(NumbersSaveOptionsNo)
    }
    
    public func saveAndQuit() {
        app.quitSaving(NumbersSaveOptionsYes)
    }
    
    public func accessDoc(docpath: String) -> SBDocumentWrapper {
        var anyObj : AnyObject? = app.open(docpath)
        var docRef: AnyObject? = app.documents().objectWithName(docpath.lastPathComponent)
        var docWrapper : SBDocumentWrapper! = nil
        if let ref : AnyObject = docRef {
            //println("Name from ref = \(ref.name!) ")
            if ref.name! != docpath.lastPathComponent {
                println("Unable to access document at \(docpath).\nName from ref = \(ref.name!) should equal \(docpath.lastPathComponent)")
                abort()
            }
            docWrapper = SBDocumentWrapper(documentRef: ref , app: self  )
        } else {
            println("Unable to open document at \(docpath)")
            abort()
        }
        return docWrapper!
    }
    
    
    public func sheetsForDoc(doc: AnyObject) -> [String:  SBRef] {
        var sheetDict =  [String:  SBRef]()
        let sheets = doc.sheets()
        if sheets.count == 0 {
            println("Document \(doc.name!) has no sheets")
            abort()
            
        }
        let sheetNames = sheets.arrayByApplyingSelector("name")
        for index in 0..<sheetNames.count  {
            var sheetName : String = sheetNames[index] as String
            var sheet : SBRef! = (sheets as SBElementArray)[index] as SBRef!
            sheetDict.updateValue(sheet, forKey: sheetName)
        }
        return sheetDict
    }
}
