//
//  SwiftScripting.swift
//  ScriptingHelper
//
//  Created by Steve Clarke on 07/06/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

import Foundation
import ScriptingBridge
import ScriptingHelperOC

public typealias SCNumbersRow = [AnyObject!]
public typealias SCMatrix = [SCNumbersRow]

public typealias SBRef = AnyObject

let ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

func nameForCell( row: Int, col: Int) -> String {
    // Only works for 26 cols so far
    assert(col < 26, "Column index must be less than 26")
    var stringStart = advance(ALPHABET.startIndex, col)
    return ALPHABET[Range(start: stringStart, end: advance(stringStart, 1))] + (row + 1).description
}

public func someValuesForRange(range : SBElementArray ) -> [AnyObject!] {
    return valuesForRange(range).map() {
        if ($0 is NSString) && ($0 as NSString) == missingSBValue() {
            return nil
        } else {
            return $0
        }
    }
}

public extension String {
    subscript (r: Range<Int>) -> String {
        get {
            let startIndex = advance(self.startIndex, r.startIndex)
            let endIndex = advance(startIndex, r.endIndex - r.startIndex)
            return self[Range(start: startIndex, end: endIndex)]
        }
    }
    func findIndex( s: String) -> Int? {
        var idx  = 0
        var currentIndex = self.startIndex
        let limit  = countElements(self) - countElements(s)
        var failed = false
        while !failed && !self[Range(start: currentIndex, end: self.endIndex)].hasPrefix(s) {
            failed = ( idx == limit)
            idx++
            currentIndex = currentIndex.successor()
        }
        if failed {
            return nil
        }else {
            return idx
        }
    }
}

extension Range {
    func asArray() -> Array<T> {
        var a = Array<T>()
        for e in self { a.append(e)}
        return a
    }
}




