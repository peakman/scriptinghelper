//
//  document_wrapper.swift
//  ScriptingHelper
//
//  Created by Steve Clarke on 26/07/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

import Foundation
import ScriptingBridge
import ScriptingHelperOC

public class SBDocumentWrapper {
    public let doc : SBRef
    public let app: ScriptingApp
    
    public init(documentRef : SBRef, app : ScriptingApp) {
        doc = documentRef
        self.app = app
    }
    
    public func sheetNamed( sheetName : String ) -> SBRef {
        let sheetRefs : [AnyObject]! = doc.sheets().filteredArrayUsingPredicate(NSPredicate(format: "name = %@", argumentArray: [sheetName]))
        if let srs : AnyObject = sheetRefs {
            if srs.count == 0  {
                println("Document \(doc.name) has no sheets")
                abort()
            }
            if let sr : SBRef =  srs[0] {
                if sr.name! != sheetName {
                    println("Got sheet named \(sr.name!) but expected \(sheetName)")
                }
                //println("Found sheet named \(sr.name!)")
                return sr
            } else {
                println("Unable to access sheet named \(sheetName)")
                abort()
            }
        } else {
            println("Unable to get sheetRefs for doc")
            abort()
        }
    }
    
    public func valuesForSheet(sheetName : String, includeHeaders: Bool = false,  tableIndex : Int = 0) -> SCMatrix {
        let sheetRef : SBRef = sheetNamed(sheetName)
        let tables = sheetRef.tables()
        let tableWrapper = wrapperFor(tables[tableIndex].name, onSheet: sheetName)
        return tableWrapper.tableValues(includeHeaders: includeHeaders)
    }
    
    public func wrapperFor(tableName: String, onSheet sheetName: String) -> SBTableWrapper {
        var tables : [AnyObject]! = sheetNamed(sheetName).tables().filteredArrayUsingPredicate(NSPredicate(format: "name = %@", argumentArray: [tableName]))
        if tables.isEmpty {
            println("Table \(tableName) not found on sheet \(sheetName)")
            abort()
        }
        return SBTableWrapper(table: tables[0])
    }
    
    
    public func updateCell( value: AnyObject! , sheetName : String, row: Int , col: Int , tableIndex : Int = 0) {
        var tableRefs : [SBRef]  = sheetNamed(sheetName).tables()
        assert(!tableRefs.isEmpty, "Sheet has no table with the index number specified ")
        ((tableRefs[tableIndex].cells().filteredArrayUsingPredicate(NSPredicate(format: "name = %@", argumentArray: [nameForCell( row, col)])))[0] as SBObject).setValue(value)
    }
    
    
    public func insertRowAfter(after: Int , sheetIndex: Int = 0 , tableIndex: Int = 0) {
        doc.sheets()[sheetIndex].tables()[tableIndex].rows()[after].addRowBelow()
    }
    public func insertRowBefore(before: Int , sheetIndex: Int = 0 , tableIndex: Int = 0) {
        doc.sheets()[sheetIndex].tables()[tableIndex].rows()[before].addRowAbove()
    }
    public func extendRows(sheetIndex: Int = 0 , tableIndex: Int = 0) {
        insertRowAfter(rowCount(sheetIndex: sheetIndex, tableIndex: tableIndex) - 1 , sheetIndex: sheetIndex, tableIndex: tableIndex)
    }
    public func rowCount(sheetIndex: Int = 0, tableIndex: Int = 0 ) -> Int {
        return doc.sheets()[sheetIndex].tables()[tableIndex].rowCount
    }
    
    public func saveAndQuit() {
        app.saveAndQuit()        
    }
    
}

