//
//  numbers_row.swift
//  ScriptingHelper
//
//  Created by Steve Clarke on 22/06/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

import Foundation
import ScriptingBridge

public enum FailureOption {
    case ABORT, WARN, QUIET
}

public class CellValueNil : CellValue {
    public required init?(_ row: Int, _ col: Int,_ wrapped: AnyObject?) {
        super.init(row, col,nil)
    }
}

public class CellValueString : CellValue {
    public required init?(_ row: Int, _ col: Int,_ wrapped: AnyObject?) {
        super.init(row, col,wrapped)
    }
    override public func isStringOpt() -> Bool {
        return true
    }
    override public func asString(fail: FailureOption = FailureOption.WARN) -> String {
        return wrapped! as String
    }
}

public class CellValueDate : CellValue {
    public required init?(_ row: Int, _ col: Int,_ wrapped: AnyObject?) {
        super.init(row, col,wrapped)
    }
    override public func isDateOpt() -> Bool {
        return true
    }
    override public func asDate(fail: FailureOption = FailureOption.WARN) -> NSDate {
        return wrapped! as NSDate
    }
}

public class CellValueDouble : CellValue {
    public required init?(_ row: Int, _ col: Int,_ wrapped: AnyObject?) {
        super.init(row, col, wrapped)
    }
    override public func isDoubleOpt() -> Bool {
        return true
    }

    override public func isBoolOpt() -> Bool {
        let dbl = wrapped! as Double
        if (dbl  == 1.0)  || (dbl == 0.0)  {
            return true
        } else {
            return false
        }
    }
    
    override public func isDecimalOpt() -> Bool {
        return true
    }
    
    override public func isIntOpt() -> Bool {
        return true
    }
    
    override public func asInt(fail: FailureOption = FailureOption.WARN) -> Int {
        return Int(wrapped! as Double)
    }
    
    override public func asBool(fail: FailureOption = FailureOption.WARN) -> Bool {
        if isBoolOpt() {
            return wrapped! as Double == 1.0
        } else {
            switch fail {
            case .ABORT:
                report("Bool", fail: .ABORT)
                return false
            case .WARN:
                report("Bool", fail: .WARN)
                return false
            case .QUIET: return false
            }
        }
    }
    override public func asDouble(fail: FailureOption = FailureOption.WARN) -> Double {
        return wrapped! as Double
    }
    override public func asDecimal(fail: FailureOption = FailureOption.WARN) -> NSDecimalNumber {
        var dnum = NSDecimalNumber(integer: Int(wrapped! as Double * 100000.0))
        return dnum.decimalNumberByDividingBy(NSDecimalNumber(integer: 100000), withBehavior: CellValue.decimalHandler)
    }
}

public class CellValue : Streamable {
    let wrapped : AnyObject?
    let row : Int
    let col : Int
    class var  decimalHandler :  NSDecimalNumberHandler {
        return NSDecimalNumberHandler(roundingMode: NSRoundingMode.RoundPlain, scale: 2, raiseOnExactness: false,
        raiseOnOverflow: true, raiseOnUnderflow: true, raiseOnDivideByZero: true) }
    public required init?(_ row: Int, _ col: Int, _ wrapped: AnyObject?) {
        self.wrapped = wrapped
        self.row = row
        self.col = col
    }
    public func writeTo<Target : OutputStreamType>(inout target: Target) {
        if let val : AnyObject = wrapped {
            target.write("\(val)")
        } else {
            target.write("nil")
        }
    }
    public func setIn(cell : SBObject) {
        cell.setValue(wrapped!)
    }
    public func isDateOpt() -> Bool {
        return false
    }
    public func isNilOpt() -> Bool {
        return false
    }
    public func isIntOpt() -> Bool {
        return false
    }
    public func isStringOpt() -> Bool {
        return false
    }
    public func isDecimalOpt() -> Bool {
        return false
    }
    public func isBoolOpt() -> Bool {
        return false
    }
    public func isDoubleOpt() -> Bool {
        return false
    }
    public func report(text: String, fail: FailureOption) {
        switch fail {
        case .ABORT:
            println("Cell at \(row),\(col) cannot be converted to \(text).  It contains \(wrapped)")
            abort()
        case .QUIET: break
        case .WARN: println("Cell at \(row),\(col) cannot be converted to \(text).  It contains \(wrapped)")
        }
    }
    public func asDecimal(fail: FailureOption = FailureOption.WARN) -> NSDecimalNumber {
        report(" Decimal", fail: fail)
        return NSDecimalNumber.zero()
    }
    public func asDate(fail: FailureOption = FailureOption.WARN) -> NSDate {
        report( "NSDate", fail: fail)
        return NSDate()
    }
    public func asString(fail: FailureOption = FailureOption.WARN) -> String {
        report( "String", fail: fail)
        return ""
    }
    public func asDouble(fail: FailureOption = FailureOption.WARN) -> Double {
        report( "Double", fail: fail)
        return 0.0
    }
    public func asBool(fail: FailureOption = FailureOption.WARN) -> Bool {
        report("Bool", fail: fail)
        return false
    }
    public func asInt(fail: FailureOption = .WARN) -> Int {
        report("Int", fail: fail)
        return 0
    }

}

public protocol PTableRow {
    init(row: SCNumbersRow, index: Int)
}


public func makeTableRows<RC : PTableRow  >(data: SCMatrix, block: ((RC)-> Void )?  = nil)  -> [RC] {
    var rows = [RC]()
    var newRow : RC
    for idx in 00..<data.count {
        newRow =  RC(row: data[idx], index : idx)
        if let cl = block {
            cl(newRow)
        } else {
            rows.append(newRow)
        }
    }
    return  rows // Will be empty if block is given
}

public class SCTableRow : PTableRow , Streamable {
    public let data : [CellValue]
    public let index : Int

    public subscript ( idx : Int) -> CellValue {
        get {
            return data[idx]
        }
    }
    public required init(row: SCNumbersRow, index: Int) {
        self.data = [CellValue]()
        self.index = index
        var cellValue  : CellValue
        for colNum in 0..<row.count {
            var value : AnyObject! = row[colNum]
            if value is NSString {
                cellValue = CellValueString(index, colNum, value)!
            } else if value is NSNumber {
                cellValue = CellValueDouble(index, colNum, value)!
            } else if value is NSDate {
                cellValue = CellValueDate(index, colNum, value)!
            } else if (value == nil) {
                cellValue = CellValueNil(index, colNum, nil)!
            } else {
                println( "Expected only dates, numbers , strings and nils")
                abort()
            }
            data.append(cellValue)
        }
    }
    public func writeTo<Target: OutputStreamType>(inout target: Target) {
        target.write("SCTableRow #\(index): [")
        for colNum in 0..<data.count {
            data[colNum].writeTo(&target)
            if colNum < data.count - 1 {
                target.write(",")
            }
        }
        target.write("]")
    }
}