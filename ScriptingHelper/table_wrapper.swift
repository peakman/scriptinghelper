//
//  table_wrapper.swift
//  oil_swift
//
//  Created by Steve Clarke on 23/07/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

import Foundation
import ScriptingBridge
import ScriptingHelperOC


public class SBTableWrapper {
    public let tabRef : SBObject
    public var tabAny : SBRef { return tabRef}
    
    public var headerRowCount : Int {
        let d : [NSObject: AnyObject]!? = tabAny.properties
        return Int(d!["headerRowCount"] as NSNumber)
    }
    public var name : String {return tabAny.name}
    
    public init(table : SBRef) {
        tabRef = table as SBObject
    }
    
    public func updateBodyCell( row: Int , col: Int , value: AnyObject! ) {
        updateCell(row + headerRowCount, col: col, value: value)
    }
    
    public func updateCell( row: Int , col: Int , value: AnyObject! ) {
        let cells: [AnyObject] = (tabAny.cells().filteredArrayUsingPredicate(NSPredicate(format: "name = %@", argumentArray: [ nameForCell(row, col)])))
        if cells.count == 0 {
            println("Cell with row \(row) and col \(col) does not exist in table \(name)")
            abort()
        }
        (cells[0] as SBObject).setValue(value)
    }
    public func insertBodyRowAfter(after: Int ) {
        insertRowAfter(after + headerRowCount)
    }
    public func insertBodyRowBefore(before: Int ) {
        insertRowBefore(before + headerRowCount)
    }
    public func insertRowAfter(after: Int ) {
        tabAny.rows()[after].addRowBelow()
    }
    public func insertRowBefore(before: Int ) {
        tabAny.rows()[before].addRowAbove()
    }
    public func extendRows() {
        insertRowAfter(tabAny.rowCount - 1)
    }
    public func rowCount() -> Int {
        return tabAny.rowCount
    }
    public func bodyRowCount() -> Int {
        return rowCount() - headerRowCount
    }
    
    public func tableValues(includeHeaders: Bool = false) -> SCMatrix {
        // I thought this would be trivial and and expression like cells().arrayByApplyingSelector("value") would work.
        // This does bring back values but only for cells that have non-nil values.  It's useless because you don't
        // know which cells these are!
        var rows : Int = tabAny.rowCount!
        var cols : Int = tabAny.columnCount!
        var rowIndexOffset : Int = 0
        if !includeHeaders && headerRowCount > 0 {
            rows -= headerRowCount
            rowIndexOffset += headerRowCount
        }
        //println("Found table named \(tabAny.name) with \(rows) rows and \(cols) columns ")
        var matrix : SCMatrix = sizedMatrix(rows, cols: cols)
        var valuesAny : [AnyObject] = valuesForRange(tabAny.cells())
        var index : Int = 0
        var value : AnyObject! = nil
        for row in rowIndexOffset..<tabAny.rowCount {
            for col in 0..<cols {
                index = col + row  * cols
                let val : AnyObject = valuesAny[index]
                if !((val is NSString) && (val as NSString) == missingSBValue()) {
                    matrix[row - rowIndexOffset][col] = val
                }
            }
        }
        return matrix
    }
    
    func sizedMatrix(rows: Int, cols:Int) -> SCMatrix {
        return (0..<rows).asArray().map() { (row : Int) in  return Array(count: cols, repeatedValue: nil) as [AnyObject!]}
    }

    func lengthOfcolumnPartOfName(name: NSString , swName: String, regex:  NSRegularExpression ) -> Int {
        // Bit of overkill to use a regex!
        return regex.firstMatchInString(name , options: NSMatchingOptions.Anchored, range: NSRange(location: 0,length: countElements(swName)))!.rangeAtIndex(1).length
    }

    public func rowIndexOf(cn: String, _ lengthOfcolumnPartOfName : Int) -> Int {
        var i = cn[Range(start: lengthOfcolumnPartOfName, end: countElements(cn))].toInt()!
        return i - 1
    }
    
    public func setColumnFormat( index: Int, format: NumbersNMCT ) {
        setRangeFormat(tabAny.columns()[index] , format)
    }

    public func columnIndexOf(cn: String, _ lengthOfcolumnPartOfName : Int) -> Int {
        var colPart = cn[0..<lengthOfcolumnPartOfName ]
        var first = true
        var highOrder = 0
        var result = 0
        for char in colPart {
            if first {
                highOrder = 0
                first = false
            } else {
                highOrder = (result + 1) * 26
            }
            result = ALPHABET.findIndex(String(char))! + highOrder
        }
        return result
    }

    func colPartExpression() -> NSRegularExpression {
        var eptr : NSErrorPointer = nil
        return NSRegularExpression(pattern: "^([\\p{Letter}]+)[\\P{Letter}]",
            options: NSRegularExpressionOptions.CaseInsensitive,  error: eptr)!
    }



}
