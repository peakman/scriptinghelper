# README #

This repository provides a Swift framework including Swift classes to wrap some ScriptingBridge classes. It has been developed to provide Swift access to Numbers documents.   

ScriptingApp wraps an SBApplication.

SBDocumentWrapper wraps a document to be accessed by SB.  I intended to make it generalised for different apps but in practice it provides support for accessing Numbers docs.

SCTableWrapper wraps a Numbers Table.

SCTableRow and the CellValue enum handle the problems of getting typed data from table cells that may contain Strings, Doubles, Dates or Nils.  (Should also do Booleans since Numbers supports these).

There are some tests that provide examples of most of the key functions.
