//
//  ScriptingHelperTests.swift
//  ScriptingHelperTests
//
//  Created by Steve Clarke on 07/06/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

import XCTest
import ScriptingBridge
import ScriptingHelperOC
import ScriptingHelper

class ScriptingHelperTests: XCTestCase {
    var resourcePath : String! {
        var  buns : [NSBundle]!  = NSBundle.allBundles().map() { $0 as NSBundle}
        buns =  buns.filter {
            if let bid = $0.bundleIdentifier {
                return bid.hasPrefix("peakit")
            } else {
                return false
            }
        }
        if buns.count > 0 {
            return buns[0].resourcePath
        }else {
            assert(false, "Bundle peakit not found")
            return nil
        }
    }
    var numbersApp : ScriptingApp {return ScriptingApp("Numbers")}
    let docname = "elec.numbers"
    var docPath : String { return  "\(resourcePath)/\(docname)"}
    let sheetName = "Input"
    var inputSheetRef : SBRef {
        var sheet: SBRef? = numbersApp.sheetsForDoc(currentDocRef)[sheetName]
        if let sht: SBRef = sheet {
            //println("Found input sheet \(sht.name!)")
            return sht
        } else {
            println("Sheet \(sheetName) not found in doc \(currentDocRef.name!)")
            abort()
        }
    }
    var inputSheet : SBObject! {
        return inputSheetRef.get() as SBObject!
    }
    var currentDoc : SBObject! {
        return currentDocRef.get() as SBObject!
    }
    var currentDocRef : SBRef {return currentDocWrapper.doc}
    var currentDocWrapper : SBDocumentWrapper { return  numbersApp.accessDoc(docPath) }
    var tableWrapper : SBTableWrapper { return currentDocWrapper.wrapperFor("Test", onSheet: "Input") }
    
 
    override func setUp() {
        super.setUp()
        //numbersApp = ScriptingApp("Numbers")
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        currentDocRef.closeSaving(NumbersSaveOptionsNo, savingIn: nil)
        //numbersApp = nil
        super.tearDown()
    }
    
    func testFailingAccessDoc() {
        XCTAssertFalse(currentDocRef.name=="xyz", "")
    }
    
    func testAccessDoc() {
        XCTAssert(currentDocRef.name==docname, "")
    }
    
    func testSheetsForDoc() {
        XCTAssert(inputSheetRef.name==sheetName, "")
    }
    
    func testRowIndexCalc() {
        XCTAssert(tableWrapper.rowIndexOf("B34",  1)==33, "")
    }

    func testColumnIndexCalc() {
        XCTAssert(tableWrapper.columnIndexOf("D34",  1)==3, "")
    }
    
    func testColumnIndexCalc2() {
        var t = tableWrapper.columnIndexOf("BB34",  2)
        XCTAssert(t==53, "")
    }
    
    
    func testDocWrapper() {
        XCTAssert(numbersApp.accessDoc(docPath).valuesForSheet("Input", includeHeaders: true).count==39, "")
    }
    
    func testSCTableRow() {
        var rows : [SCTableRow] = makeTableRows( numbersApp.accessDoc(docPath).valuesForSheet("Input", includeHeaders: false))
        XCTAssert(rows.count == 38)
        var tdate = NSDate.dateWithNaturalLanguageString("26 Feb 2010 00:00:00") as NSDate
        var numbersDate  = rows[0][0].asDate()
        XCTAssert(numbersDate.isEqualToDate(tdate), "")
    }
    
    func testCellStreaming() {
        var rows : [SCTableRow] = makeTableRows( numbersApp.accessDoc(docPath).valuesForSheet("Input", includeHeaders: true))
        var val: AnyObject! = ((currentDocWrapper.doc.sheets()[0].tables()[0].cells()[0] as SBObject).value() as SBObject).get()
        var strStream : String = ""
        rows[0][0].writeTo(&strStream)
        XCTAssert(strStream == "\(val)", "")
    }
    
    func testTableRowStreaming() {
        var rows : [SCTableRow] = makeTableRows( numbersApp.accessDoc(docPath).valuesForSheet("Input", includeHeaders: false))
        var strStream : String = ""
        rows[0].writeTo(&strStream)
        println(strStream)
        XCTAssert(strStream != "", "")
    }
    
    func testCellTypeCheckLenient() {
        var rows : [SCTableRow] = makeTableRows( numbersApp.accessDoc(docPath).valuesForSheet("Input", includeHeaders: false))
        var tdate = NSDate.dateWithNaturalLanguageString("26 Feb 2010 00:00:00") as NSDate
        var numbersDate  = rows[0][0].asDate()
        var badString = rows[0][0].asString() // Should cause warning message to be output and empty string returned
        XCTAssert(badString == "", "")
    }
    
    func testCellTypeQuiet() {
        var rows : [SCTableRow] = makeTableRows( numbersApp.accessDoc(docPath).valuesForSheet("Input", includeHeaders: false))
        var badString = rows[0][0].asString(fail: .QUIET) // Should cause no warning message to be output and empty string returned
        XCTAssert(badString == "", "")
    }
    
    func testCellTypeCheckStrict() {
        var rows : [SCTableRow] = makeTableRows( numbersApp.accessDoc(docPath).valuesForSheet("Input", includeHeaders: false))
        var tdate = NSDate.dateWithNaturalLanguageString("26 Feb 2010 00:00:00") as NSDate
        var numbersDate  = rows[0][0].asDate()
        var badString = rows[0][0].asString(fail: .ABORT) // Should cause test to abort
        XCTAssert(badString == "", "")
    }
    
    func testCellBoolStrictAbort() {
        var rows : [SCTableRow] = makeTableRows( numbersApp.accessDoc(docPath).valuesForSheet("Input", includeHeaders: false))
        XCTAssert(rows[2][17].asBool(fail: .ABORT), "")  // Should NOT output warning, should not abort
    }
    
    func testCellBoolStrictTrue() {
        var rows : [SCTableRow] = makeTableRows( numbersApp.accessDoc(docPath).valuesForSheet("Input", includeHeaders: false))
        XCTAssert(rows[2][17].asBool(fail: .ABORT), "") // Should NOT output warning
    }
    
    func testCellBoolLenientDefault() {
        var rows : [SCTableRow] = makeTableRows( numbersApp.accessDoc(docPath).valuesForSheet("Input", includeHeaders: false))
        XCTAssertFalse(rows[3][16].asBool(), "") // Should print warning
    }
    
    func testCellBoolLenientTrue() {
        var rows : [SCTableRow] = makeTableRows( numbersApp.accessDoc(docPath).valuesForSheet("Input", includeHeaders: false))
        XCTAssert(rows[2][17].asBool(), "") // Should NOT output warning
    }
    
    func testCellUpdate() {
        var tdate = NSDate.dateWithNaturalLanguageString("26 Feb 2010") as NSDate
        var testString = "Test String"
        currentDocWrapper.updateCell( testString ,sheetName : sheetName, row: 0, col: 0, tableIndex : 0 )
        var val: AnyObject! = ((currentDocWrapper.doc.sheets()[0].tables()[0].cells()[0] as SBObject).value() as SBObject).get()
        XCTAssert(testString == (val as String), "")
    }
    
    func testUpdateCellInTable() {
        var docWrapper : SBDocumentWrapper = numbersApp.accessDoc(docPath)
        var tabWrapper : SBTableWrapper = docWrapper.wrapperFor("Test", onSheet: "Input")
        tabWrapper.updateBodyCell(0, col: 0, value: "")
        let testString = "Test String"
        tabWrapper.updateBodyCell(0, col: 0, value: testString)
        var rows : [SCTableRow] = makeTableRows(docWrapper.valuesForSheet(sheetName, includeHeaders: false, tableIndex: 1))
        XCTAssert(testString == rows[0][0].asString(), "")
    }
    
    func testGetTableWrapper() {
        let tabWrapper = numbersApp.accessDoc(docPath).wrapperFor("Test", onSheet: "Input")
        XCTAssert(tabWrapper.tabAny.columnCount > 0 )
    }
    
    func testSetColumnFormat() {
        let tabWrapper = numbersApp.accessDoc(docPath).wrapperFor("Test", onSheet: "Input")
        tabWrapper.setColumnFormat(1, format: NumbersNMCTCurrency)
        let colRange : AnyObject = tabWrapper.tabAny.columns()[1]
        //let dict : [NSObject : AnyObject] = colRange.properties
        //println(dict)
        XCTAssert(tabWrapper.tabAny.columnCount > 0 )
    }
    
}
